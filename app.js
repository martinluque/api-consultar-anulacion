const fetch = require("node-fetch");

const conexion = () => {
  const { Pool } = require("pg");
  var pool = new Pool({
    user: "syzgjuxrvlvkwc",
    password:
      "9a898e6b87b09028528dab653babf2247c599e301529f8e18977b6a3b507a565",
    database: "d8h18fe5g1jjr1",
    port: 5432,
    host: "ec2-3-211-176-230.compute-1.amazonaws.com",
    ssl: { rejectUnauthorized: false },
  });
  return pool;
};

function responseMessage(results) {
  let statusCode = 200;
  let data = results;
  let success = true;

  return {
    statusCode,
    headers: {
      "Access-Control-Allow-Headers": "*",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      "Access-Control-Allow-Credentials": true,
      Accept: "*/*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ success, data }),
  };
}

module.exports.handler = async (event, context) => {
  let requestBody = JSON.parse(event.body);

  let nro_documento_api = requestBody.nro_documento;
  let tipo_documento_api = requestBody.tipo_documento;
  let idsuscripcion_api = requestBody.idsuscripcion;

  var url = 'https://landingpage-comercio.herokuapp.com/api/consulta-anulacion';
  var data = { nro_documento_api: nro_documento_api, tipo_documento_api: tipo_documento_api, idsuscripcion_api: idsuscripcion_api };

  var message = "";
  var code = "";
  var landing = "";

  const response = await new Promise((rsv, rjt) => {
    try {
      fetch(url, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => {


          let len = response['message'].length;
          let estado = response['message'][0]['estado'];
          let estado_anulacion = response['message'][0]['estadoanulacion'];

          if (len > 0 && estado == 'Activo' && estado_anulacion == 'Ninguna') {
            message = "Debe comunicarse al area de retenciones 31150000";
            code = "1";
          } else if (len > 0 && estado == 'Activo' && estado_anulacion == 'Pendiente') {
            message = anulacion.rows[0]['fechaanulacion'];
            code = "2";
          } else if (len > 0 && estado == 'Inactivo' && estado_anulacion == 'Terminada') {
            message = anulacion.rows[0]['fechaanulacion'];
            code = "3";
          } else {
            message = "No se encontro ningun informacion de Anulacion";
            code = "0";
          }


          rsv({
            message,
            code,
          });



        });
    } catch (error) {
      rjt({

        message: error.message,
        code: '0'
      });
    }
  });

  return responseMessage(response);
};
